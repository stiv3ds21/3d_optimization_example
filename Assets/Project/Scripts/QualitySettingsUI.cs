using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class QualitySettingsUI : MonoBehaviour
{
    [SerializeField] private QualityToggleItem _qualityTogglePrefabe;
    [SerializeField] private Transform _parentQuality;

    private List<QualityToggleItem> _temp = new();

    private void Awake()
    {
        _qualityTogglePrefabe.gameObject.SetActive(false);
        var listQuality = QualitySettings.names.ToList();
        listQuality.ForEach(x =>
        {
            var item = Instantiate(_qualityTogglePrefabe, _parentQuality);
            item.OnChangeToggle += ToggleHandler;
            item.Init(x);
            item.gameObject.SetActive(true);
            _temp.Add(item);
        });
        var current = QualitySettings.GetQualityLevel();
        ToggleHandler(_temp[current], true);
    }

    private void ToggleHandler(QualityToggleItem item, bool obj)
    {
        _temp.ForEach(x => x.SetValue(x.NameQuality == item.NameQuality));
        QualitySettings.SetQualityLevel(_temp.IndexOf(item), false);
    }
}