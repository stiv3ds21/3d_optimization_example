using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QualityToggleItem : MonoBehaviour
{
    public event Action<QualityToggleItem, bool> OnChangeToggle = delegate { };

    [SerializeField] private TMP_Text _nameQualityTxt;
    
    private Toggle _toggle;
    private string _nameQuality;

    public string NameQuality => _nameQuality;

    private void Awake()
    {
        _toggle = GetComponent<Toggle>();
        _toggle.onValueChanged.AddListener(ToggleHandler);
    }

    private void ToggleHandler(bool isValue)
    {
        OnChangeToggle(this, isValue);
    }

    public void Init(string nameQuality)
    {
        _nameQuality = nameQuality;
        _nameQualityTxt.SetText(_nameQuality);
    }

    public void SetValue(bool isValue)
    {
        _toggle.SetIsOnWithoutNotify(isValue);
    }
}